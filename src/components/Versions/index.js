import React, { useEffect, useState } from 'react';
import Styled from './style';

const Versions = ({ data }) => {
  const [versions, setVersions] = useState([]);
  const [entrie, setEntrie] = useState();

  const versionSelectHandler = (version) => {
    setEntrie(versions[version]);
  };

  useEffect(() => {
    if (data) {
      const twoFirstVersions = data.slice(0, 2);
      setVersions(twoFirstVersions);
      setEntrie(twoFirstVersions[0]);
    }
  }, [data]);

  return (
    <Styled>
      <div className="text-entrie">{entrie}</div>
      <div className="tool-selector-container">
        <div className="tool-selector-title">Versiones:</div>
        <div className="tool-selector-buttons">
          {versions.map((x, index) => (
            <div
              className="pokeball"
              onClick={() => versionSelectHandler(index)}
              key={index}
            ></div>
          ))}
        </div>
      </div>
    </Styled>
  );
};

export default React.memo(Versions);
