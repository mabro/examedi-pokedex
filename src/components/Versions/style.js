import styled from 'styled-components';

const Styled = styled.div`
  margin-bottom: 20px;
  .text-entrie {
    color: #212121;
    font-size: 112.5%;
    line-height: 150%;
    margin-bottom: 10px;
  }
  .tool-selector-container {
    display: flex;
    flex-direction: row;
    align-items: center;
    .tool-selector-buttons {
      display: flex;
      .pokeball {
        cursor: pointer;
        margin-left: 10px;
      }
    }
  }
`;

export default Styled;
