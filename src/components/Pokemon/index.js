import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { usePokemonData } from '../../hooks/usePokemonData';
import Image from '../Image';
import Information from '../Information';
import Styled from './style';

const Pokemon = ({ data }) => {
  const { image, types, number, name } = usePokemonData(data);

  return (
    <Styled as={Card}>
      <Link to={`/pokedex/${number}`}>
        <Image src={image} alt={name} />
      </Link>
      <Information name={name} number={number} types={types} />
    </Styled>
  );
};

export default React.memo(Pokemon);
