import styled from 'styled-components';

const Styled = styled.div`
  border: unset;
  margin-bottom: 2rem;
  &:hover {
    animation-name: poing;
    animation-duration: 0.2s;
  }
  img {
    background: #f2f2f2;
  }
`;

export default Styled;
