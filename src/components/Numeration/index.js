import React from 'react';
import Styled from './style';

const Numeration = ({ number }) => {
  return (
    <Styled>
      <span>N.º</span>
      {String(number).padStart(3, '0')}
    </Styled>
  );
};

export default React.memo(Numeration);
