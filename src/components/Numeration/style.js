import styled from 'styled-components';

const Styled = styled.div`
  font-family: 'Flexo-Bold', arial, sans-serif;
  color: #919191;
  font-size: 80%;
`;

export default Styled;
