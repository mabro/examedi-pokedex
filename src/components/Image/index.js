import React from 'react';
import Styled from './style';

const Image = (props) => {
  return <Styled className="card-img-top" {...props} />;
};

export default React.memo(Image);
