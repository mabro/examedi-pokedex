import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Column from './components/Column';
import Styled from './style';

const Stats = ({ data }) => {
  return (
    <Styled>
      <div className="title">Puntos de base</div>
      <div className="stats-container">
        <Row>
          {data.map((x, index) => (
            <Col key={index}>
              <Column data={x.base_stat} />
              <div className="stat-name">{x.stat.name}</div>
            </Col>
          ))}
        </Row>
      </div>
    </Styled>
  );
};

export default React.memo(Stats);
