import React, { useEffect, useState } from 'react';
import { StyledBlock } from './style';

const LEVELS = 15;
const MAX_STAT = 252;

// eslint-disable-next-line no-unused-vars
const Column = ({ data }) => {
  const [columns, setColumns] = useState([]);

  useEffect(() => {
    const stat_level = data / (MAX_STAT / LEVELS);
    const ceilStat = Math.ceil(stat_level);
    const coloredColumns = [...Array(15).keys()].map((_, index) => ({
      color: index < ceilStat
    }));
    setColumns(coloredColumns.reverse());
  }, [data]);

  return columns.map((x, index) => (
    <StyledBlock className="block" key={index} {...x}></StyledBlock>
  ));
};

export default React.memo(Column);
