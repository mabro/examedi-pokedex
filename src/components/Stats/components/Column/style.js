import styled from 'styled-components';

const Styled = styled.div``;

export const StyledBlock = styled.div`
  height: 8px;
  width: 100%;
  margin-bottom: 4px;
  background-color: ${({ color }) => (color ? '#30a7d7' : 'white')};
`;

export default Styled;
