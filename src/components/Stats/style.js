import styled from 'styled-components';

const Styled = styled.div`
  background-color: #a4a4a4;
  border-radius: 10px;
  display: block;
  margin: 1rem 0;
  width: 100%;
  & .stats-container {
    padding: 0.3rem 2.5rem 1rem 2.5rem;
  }
  & .title {
    padding: 0.8rem 1rem;
    color: #313131;
    margin-bottom: 10px;
  }
  & .stat-name {
    font-weight: bold;
    color: #212121;
    float: left;
    font-size: 62.5%;
    text-align: center;
    text-transform: capitalize;
    width: 100%;
  }
  & .col {
    padding: 0 3px;
  }
`;

export default Styled;
