import React from 'react';
import { usePokemonElement } from '../../hooks/usePokemonElement';
import Styled from './style';

const Type = ({ children, size }) => {
  const { name } = usePokemonElement(children);
  return (
    <Styled lg="6" className={children} size={size}>
      {name}
    </Styled>
  );
};

export default React.memo(Type);
