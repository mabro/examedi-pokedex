import React from 'react';
import Numeration from '../Numeration';
import Types from '../Types';
import Styled, { StyledName } from './style';

const Information = ({ name, number, types }) => {
  return (
    <Styled className="card-body">
      <Numeration number={number} />
      <StyledName className="h5">{name}</StyledName>
      <Types types={types} />
    </Styled>
  );
};

export default React.memo(Information);
