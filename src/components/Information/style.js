import styled from 'styled-components';

const Styled = styled.div`
  padding: 2px 1rem;
`;
export const StyledName = styled.div`
  color: #313131;
  text-transform: capitalize;
  margin-top: 0.5rem;
  margin-bottom: 5px;
`;

export default Styled;
