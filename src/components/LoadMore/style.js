import styled from 'styled-components';

const Styled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export default Styled;
