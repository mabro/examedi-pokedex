import React, { useEffect, useRef, useState } from 'react';
import useScroll from '../../hooks/useScroll';
import usePokedexStore from '../../store/pokedex';
import Button from '../../ui/Button';
import Styled from './style';

const LoadMore = ({ loadMore }) => {
  const ref = useRef(null);
  const { buttonVisibility, setButtonVisibility } = usePokedexStore();
  const scrollPosition = useScroll();
  const [loading, setLoading] = useState(false);
  const [atBottom, setAtBottom] = useState(false);

  const loadPokemon = () => {
    setLoading(true);
    setTimeout(() => {
      loadMore();
      setLoading(false);
      setButtonVisibility(false);
    }, 1000);
  };

  const loadMoreHandler = () => {
    loadPokemon();
  };

  useEffect(() => {
    const isAtBottom =
      ref.current.getBoundingClientRect().bottom <= window.innerHeight + 100;
    setAtBottom(isAtBottom);
  }, [scrollPosition]);

  useEffect(() => {
    if (!buttonVisibility && atBottom) {
      loadPokemon();
    }
  }, [atBottom]);

  return (
    <Styled ref={ref}>
      {loading ? <div className="pokeball rotating" /> : null}
      <Button
        visibility={buttonVisibility && !loading}
        onClick={loadMoreHandler}
      >
        Cargar más Pokémon
      </Button>
    </Styled>
  );
};

export default LoadMore;
