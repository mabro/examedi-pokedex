import styled from 'styled-components';

const Styled = styled.div`
  display: flex;
  justify-content: center;
  & .row {
    gap: 24px 0px;
    max-width: 1024px;
    padding: 0 2rem;
  }
`;

export default Styled;
