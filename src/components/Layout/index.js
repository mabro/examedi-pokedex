import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Pokemon from '../Pokemon';
import Styled from './style';

const Layout = ({ data }) => {
  return (
    <Styled>
      <Row>
        {data?.map((props, index) => (
          <Col key={index} xs="12" sm="6" md="4" lg="3">
            <Pokemon data={props} />
          </Col>
        ))}
      </Row>
    </Styled>
  );
};

export default Layout;
