import styled from 'styled-components';

export const Styled = styled.div`
  background-color: #30a7d7;
  color: #fff;
  border-radius: 10px;
  width: 100%;
  padding: 5px;
  .col-lg-6 {
    display: flex;
    flex-direction: column;
  }
  .gender {
    display: flex;
    & > div {
      background-size: 20px;
    }
  }
`;

export const StyledEntrie = styled.div`
  width: 100%;
  display: contents;
  .title {
    color: #fff;
    float: left;
  }
  .value {
    color: #212121;
    float: left;
    margin-bottom: 5px;
  }
`;

export default Styled;
