import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { usePokemonData } from '../../hooks/usePokemonData';
import { usePokemonSpecie } from '../../hooks/usePokemonSpecie';
import Styled, { StyledEntrie } from './style';

const Info = ({ title, value }) => {
  return (
    <StyledEntrie>
      <div className="title">{title}</div>
      <div className="value">{value}</div>
    </StyledEntrie>
  );
};

const PokemonInfo = ({ data, specie }) => {
  const { weight, height, ability } = usePokemonData(data);
  const { gender, genera } = usePokemonSpecie(specie);

  return (
    <Styled as={Row}>
      <Col lg="6">
        <Info title="Altura" value={`${height} m`} />
        <Info title="Peso" value={`${weight} kg`} />
        <Info
          title="Sexo"
          value={
            gender >= 0 ? (
              <div className="gender">
                <div className="male" />
                <div className="female" />
              </div>
            ) : (
              'Desconocido'
            )
          }
        />
      </Col>
      <Col lg="6">
        <Info title="Categorīa" value={genera} />
        <Info title="Habilidad" value={ability?.name} />
      </Col>
    </Styled>
  );
};

export default React.memo(PokemonInfo);
