import React from 'react';
import Type from '../Type';
import Styled from './style';

const Types = ({ types, size }) => {
  return (
    <Styled>
      {types?.map?.((x, index) => (
        <Type key={index} size={size}>
          {x}
        </Type>
      ))}
    </Styled>
  );
};

export default React.memo(Types);
