import styled from 'styled-components';

const Styled = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export default Styled;
