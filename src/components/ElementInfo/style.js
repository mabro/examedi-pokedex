import styled from 'styled-components';

export const StyledElementInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StyledElementType = styled.div`
  display: flex;
  flex-direction: column;
  .title {
    color: #212121;
    float: left;
    margin-top: 1.25em;
    text-transform: capitalize;
    width: 100%;
    font-size: 125%;
    line-height: 125%;
  }
`;
