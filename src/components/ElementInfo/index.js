/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import { usePokemonWeakness } from '../../hooks/usePokemonWeakness';
import Types from '../Types';
import Styled, { StyledElementInfo, StyledElementType } from './style';

const ElementType = ({ title, types }) => {
  return (
    <StyledElementType>
      <h3 className="title">{title}</h3>
      <Types types={types} size="lg" />
    </StyledElementType>
  );
};

const ElementInfo = ({ data }) => {
  const { weakness } = usePokemonWeakness(data);

  return (
    <StyledElementInfo>
      {[
        { title: 'Tipo', types: data },
        { title: 'Debilidad', types: weakness }
      ].map(({ title, types }, index) => (
        <ElementType key={index} title={title} types={types} />
      ))}
    </StyledElementInfo>
  );
};

export default React.memo(ElementInfo);
