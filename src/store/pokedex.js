import create from 'zustand';
import { getData } from '../service/pokedex';

const usePokedexStore = create((set, get) => ({
  collection: [],
  error: null,
  loading: false,
  nextLink: null,
  allowFetch: true,
  buttonVisibility: true,
  fetchData: async () => {
    if (get().allowFetch) {
      set({ loading: true });
      try {
        const { next, results } = await getData(get().nextLink);
        const pokemonLinks = results.map((x) => getData(x.url));
        const response = await Promise.all(pokemonLinks);
        const defaultPokemons = response.filter((x) => x.is_default);
        set({
          nextLink: next,
          collection: [...get().collection, ...defaultPokemons],
          loading: false,
          allowFetch: defaultPokemons.length === 12
        });
      } catch (error) {
        set({ error, loading: false });
      }
    }
  },
  setButtonVisibility: (buttonVisibility) => {
    set({ buttonVisibility });
  }
}));

export default usePokedexStore;
