import create from 'zustand';
import { getType } from '../service/pokedex';

const useTypeStore = create((set, get) => ({
  collection: {},
  setCollection: (collection) => {
    set({ collection });
  },
  getType: async (type) => {
    const types = get().collection;
    if (types[type]) {
      return types[type];
    } else {
      try {
        const result = await getType(type);
        set({
          collection: {
            ...types,
            [type]: result
          }
        });
        return result;
      } catch (error) {
        return 'Undefined';
      }
    }
  }
}));

export default useTypeStore;
