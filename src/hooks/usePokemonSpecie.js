import { useEffect, useState } from 'react';
import { LANGUAGE } from '../config/constants';

export const usePokemonSpecie = (data) => {
  const [textEntries, setTextEntries] = useState(null);
  const [gender, setGender] = useState();
  const [genera, setGenera] = useState();

  useEffect(() => {
    if (data) {
      setTextEntries(
        data.flavor_text_entries
          ?.filter((x) => x.language.name === LANGUAGE)
          ?.map((x) => x.flavor_text)
      );
      setGender(data.gender_rate);
      setGenera(data?.genera?.find((x) => x.language.name === LANGUAGE)?.genus);
    }
  }, [data]);

  return { textEntries, gender, genera };
};
