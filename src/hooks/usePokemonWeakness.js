/* eslint-disable no-unused-vars */
import { useEffect, useState } from 'react';
import { getData } from '../service/pokedex';

export const usePokemonWeakness = (types) => {
  const [typeWeakness, setTypeWeakness] = useState(null);

  useEffect(() => {
    if (types?.length) {
      const getPokemonWeakness = async (types) => {
        const pokemonTypePromises = types.map((x) => getData(`/type/${x}`));
        const result = await Promise.all(pokemonTypePromises);
        const weaknessByType = result.reduce(
          (acc, elem) => [
            ...acc,
            ...elem.damage_relations?.double_damage_from.map((x) => x.name)
          ],
          []
        );
        const weaknessByTypes = [...new Set(weaknessByType)];
        const resistacesByType = result.reduce(
          (acc, elem) => [
            ...acc,
            ...elem.damage_relations?.half_damage_from.map((x) => x.name)
          ],
          []
        );
        const noDamage = result.reduce(
          (acc, elem) => [
            ...acc,
            ...elem.damage_relations?.no_damage_from.map((x) => x.name)
          ],
          []
        );
        const resistance = [...new Set([...resistacesByType, ...noDamage])];
        setTypeWeakness(weaknessByTypes.filter((x) => !resistance.includes(x)));
      };
      getPokemonWeakness(types);
    }
  }, [types]);

  return { weakness: typeWeakness };
};
