import { useEffect, useState } from 'react';
import { LANGUAGE } from '../config/constants';
import { getData } from '../service/pokedex';

export const usePokemonData = (data) => {
  const [name, setName] = useState(null);
  const [image, setImage] = useState(null);
  const [number, setNumber] = useState(null);
  const [types, setTypes] = useState([]);
  const [stats, setStats] = useState([]);
  const [height, setHeight] = useState(null);
  const [weight, setWeight] = useState(null);
  const [ability, setAblility] = useState(null);

  useEffect(() => {
    if (data) {
      setName(data.name);
      setNumber(data.id);
      setImage(data.sprites.other['official-artwork'].front_default);
      const types_ = data.types.map((x) => x.type.name);
      setTypes(types_);
      setStats(data.stats);
      setHeight(data.height / 10);
      setWeight(data.weight / 10);
      const abilityUrl = data.abilities?.at(0)?.ability?.url;
      getData(abilityUrl).then((response) => {
        const name = response?.names?.find(
          (x) => x.language.name === LANGUAGE
        )?.name;
        const description = response?.flavor_text_entries?.find(
          (x) => x.language.name === LANGUAGE
        )?.flavor_text;
        setAblility({ name, description });
      });
    }
  }, [data]);

  return { image, types, number, name, stats, height, weight, ability };
};
