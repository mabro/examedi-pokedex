import { useEffect, useState } from 'react';
import useTypeStore from '../store/types';

export const usePokemonElement = (type) => {
  const [typeName, setTypeName] = useState(null);
  const { getType } = useTypeStore();

  useEffect(() => {
    if (type) {
      const getPokemonTypeName = async (type) => {
        getType(type).then((result) => {
          setTypeName(result);
        });
      };
      getPokemonTypeName(type);
    }
  }, [type]);

  return { name: typeName };
};
