import { LANGUAGE } from '../config/constants';
import http from '../utils/http';

const INITIAL_LINK = '/pokemon?offset=0&limit=12';

export const getData = async (url) => {
  try {
    const result = await http.get(url || INITIAL_LINK);
    return result.data;
  } catch (error) {
    return { error };
  }
};

export const getType = async (typeId) => {
  try {
    const result = await http.get(`/type/${typeId}`);
    return result.data.names?.find((x) => x.language.name === LANGUAGE)?.name;
  } catch (error) {
    return { error };
  }
};

export const fillTypes = async () => {
  try {
    const typeSourceList = await getData('/type');
    const typePromise = typeSourceList?.results?.map((x) => getData(x.url));
    const typeDataList = await Promise.all(typePromise);
    const result = typeDataList.reduce(
      (acc, elem) => ({
        ...acc,
        [elem.name]: elem.names?.find((x) => x.language.name === LANGUAGE)?.name
      }),
      {}
    );
    return result;
  } catch (error) {
    return error;
  }
};
