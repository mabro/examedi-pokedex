import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Pokedex from './pages/Pokedex';
import { Switch, Route, Redirect } from 'react-router-dom';
import Target from './pages/Target';
import { Container } from 'react-bootstrap';
import useTypeStore from './store/types';
import { fillTypes } from './service/pokedex';

function App() {
  const { setCollection } = useTypeStore();
  useEffect(() => {
    fillTypes().then((result) => setCollection(result));
  }, []);
  return (
    <Container>
      <Switch>
        <Route exact path="/pokedex" component={Pokedex} />
        <Route path="/pokedex/:id" component={Target} />
        <Redirect to="/pokedex" />
      </Switch>
    </Container>
  );
}

export default App;
