import React, { useEffect } from 'react';
import Layout from '../../components/Layout';
import LoadMore from '../../components/LoadMore';
import usePokedexStore from '../../store/pokedex';
import Styled from './style';

const Pokedex = () => {
  const { collection, fetchData } = usePokedexStore();

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Styled>
      <Layout data={collection} />
      <LoadMore loadMore={fetchData} />
    </Styled>
  );
};

export default Pokedex;
