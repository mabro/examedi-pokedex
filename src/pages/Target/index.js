import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useParams } from 'react-router';
import ElementInfo from '../../components/ElementInfo';
import Image from '../../components/Image';
import Numeration from '../../components/Numeration';
import PokemonInfo from '../../components/PokemonInfo';
import Stats from '../../components/Stats';
import Versions from '../../components/Versions';
import { usePokemonData } from '../../hooks/usePokemonData';
import { usePokemonSpecie } from '../../hooks/usePokemonSpecie';
import { getData } from '../../service/pokedex';
import Styled, { StyledTitleName } from './style';

const Target = () => {
  const { id } = useParams();
  const [target, setTarget] = useState();
  const [specie, setSpecie] = useState();
  const { image, name, stats, types, number } = usePokemonData(target);
  const { textEntries } = usePokemonSpecie(specie);

  useEffect(() => {
    getData(`/pokemon/${id}`).then((result) => {
      setTarget(result);
    });
    getData(`/pokemon-species/${id}`).then((result) => {
      setSpecie(result);
    });
  }, []);

  return (
    <Styled>
      <StyledTitleName>
        <span className="pokemon-name">{name}</span>
        <Numeration number={number} />
      </StyledTitleName>
      <Row>
        <Col>
          <Image src={image} alt={name} />
          <Stats data={stats} />
        </Col>
        <Col>
          <Versions data={textEntries} />
          <PokemonInfo data={target} specie={specie} />
          <ElementInfo data={types} />
        </Col>
      </Row>
    </Styled>
  );
};

export default Target;
