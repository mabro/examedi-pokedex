import styled from 'styled-components';

const Styled = styled.div`
  .pokemon-name {
    text-transform: capitalize;
  }
`;

export const StyledTitleName = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 25px;
  padding: 10px 0px;
  .pokemon-name {
    color: #212121;
    margin-right: 5px;
  }
`;

export default Styled;
