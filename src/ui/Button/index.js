import React from 'react';
import { Button } from 'react-bootstrap';
import Styled from './style';

const ButtonComponent = ({ children, ...rest }) => {
  return (
    <Styled {...rest} as={Button}>
      {children}
    </Styled>
  );
};

export default React.memo(ButtonComponent);
