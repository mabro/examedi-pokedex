import styled from 'styled-components';

const Styled = styled.button`
  display: inline-block;
  margin: 0 auto;
  font-size: 100%;
  border-radius: 5px;
  border: none;
  cursor: pointer;
  line-height: 125%;
  margin: 1.5625%;
  padding: 0.75rem 1.2rem 0.675rem;
  vertical-align: middle;
  text-align: center;
  text-transform: none;
  background-color: #30a7d7;
  color: #fff;
  &:hover {
    background-color: #1b82b1;
  }
  visibility: ${({ visibility }) => (visibility ? 'visible' : 'hidden')};
`;

export default Styled;
