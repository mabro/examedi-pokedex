import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    height: 100%;
  }

  #root {
    height: 100%;
  }

  .pokeball { 
    height: 35px;
    width: 35px;
    background: url(/images/pokeball.svg) no-repeat left top;
  }

  .male { 
    height: 32px;
    width: 32px;
    background: url(/images/male.png) no-repeat left top;
  }

  .female { 
    height: 32px;
    width: 32px;
    background: url(/images/female.png) no-repeat left top;
  }

  @keyframes rotating {
    from {
      -ms-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -webkit-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    to {
      -ms-transform: rotate(360deg);
      -moz-transform: rotate(360deg);
      -webkit-transform: rotate(360deg);
      -o-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  .rotating {
    -webkit-animation: rotating 0.35s linear infinite;
    -moz-animation: rotating 0.35s linear infinite;
    -ms-animation: rotating 0.35s linear infinite;
    -o-animation: rotating 0.35s linear infinite;
    animation: rotating 0.35s linear infinite;
  }

  @keyframes poing {
    0% {
      -ms-transform: translateY(0px);
      -moz-transform: translateY(0px);
      -webkit-transform: translateY(0px);
      -o-transform: translateY(0px);
      transform: translateY(0px);
    }
    50% {
      -ms-transform: translateY(-5px);
      -moz-transform: translateY(-5px);
      -webkit-transform: translateY(-5px);
      -o-transform: translateY(-5px);
      transform: translateY(-5px);
    }
    100% {
      -ms-transform: translateY(0px);
      -moz-transform: translateY(0px);
      -webkit-transform: translateY(0px);
      -o-transform: translateY(0px);
      transform: translateY(0px);
    }
  }
`;

export default GlobalStyle;
