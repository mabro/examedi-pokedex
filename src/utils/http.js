import axios from 'axios';
import { axiosConfig } from '../config/axios';

const axiosInstance = axios.create(axiosConfig);

const get = async (url, params) => {
  return await axiosInstance.get(url, { params });
};

const post = async (url, data, params) => {
  return await axiosInstance.post(url, data, { params });
};

const put = async (url, data, params) => {
  return await axiosInstance.put(url, data, { params });
};

const _delete = async (url, id) => {
  return await axiosInstance.delete(`${url}/${id}`);
};

export default {
  get,
  post,
  put,
  delete: _delete
};
