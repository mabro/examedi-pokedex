export const axiosConfig = {
  baseURL: 'https://pokeapi.co/api/v2',
  timeout: 60000,
  responseType: 'json',
  headers: {
    Accept: 'application/json'
  }
};
